<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/
    Auth::routes();
    
    // Route::get('/clear-cache', function() {
    //     $exitCode = Artisan::call('config:clear');
    //     $exitCode = Artisan::call('cache:clear');
    //     $exitCode = Artisan::call('config:cache');
    //     return 'DONE'; //Return anything
    // });
    
    /*
    |==========================================|
    | Route groups for                         |
    | --prefix secret/admin                    |
    |==========================================|
    */
    Route::middleware(['admin'])->prefix('admin')->namespace('Admin')->group(function()
    {
        Route::get('/',['as'=>'admin','uses'=>'AdminController@index']);
        /*
        |==========================================|
        | Route groups for image slider            |
        |==========================================|
        */
        Route::group(['prefix' => 'image_slider' ], function ()
        {
            Route::get('/','ImageSliderController@listimages');
            Route::get('add_image_slider','ImageSliderController@addImages');
            Route::post('add_image_slider','ImageSliderController@store');
            Route::post('edit','ImageSliderController@edit');
            Route::delete('delete/{sub_cat_id}','ImageSliderController@delete');
        });
        /*
        |==========================================|
        | Route groups for image pages             |
        |==========================================|
        */

        // Route::group(['prefix' => 'pages'], function ()
        // {
        //     Route::get('/error',function(){ return view('admin.pages.404'); });
        //     Route::get('/register',function(){ return view('admin.pages.register'); });
        //     Route::get('/login',function(){ return view('admin.pages.login'); });
        //     Route::get('/cards',function(){ return view('admin.pages.cards'); });
        // });

        /*
        |==========================================|
        | Route groups for users-details           |
        |==========================================|
        */
        // Route::get('/users-details',function(){
        //     return view('admin.pages.users-details');
        // });

        /***
        |==========================================|
        | Route groups for top-categories          |
        |==========================================|
        */
        // Route::group(['prefix'=> 'top_categories'],function ()
        // {
        //     Route::get('/','TopCategoryController@index');
        //     Route::get('add_top_categories','TopCategoryController@add');
        //     Route::post('add_top_categories','TopCategoryController@create');
        //     Route::post('edit','TopCategoryController@update');
        //     Route::delete('delete/{cat_id}','TopCategoryController@destroy');
        // });

        /***
        |==========================================|
        | Route groups for sub-categories          |
        |==========================================|
        */
        // Route::group(['prefix' => 'sub_categories'], function ()
        // {
        //     Route::get('/','SubCategoryController@index');
        //     Route::get('add_sub_categories','SubCategoryController@add');
        //     Route::post('add_sub_categories','SubCategoryController@create');
        //     Route::post('edit','SubCategoryController@update');
        //     Route::delete('delete/{sub_cat_id}','SubCategoryController@destroy');
        // });
        /*
        |==========================================|
        | Route groups for sub-categories          |
        |==========================================|
        */
        Route::group(['prefix' => 'items'], function ()
        {
            Route::get('/','ItemsController@index');
            Route::get('add_items','ItemsController@add');
            Route::post('add_items','ItemsController@create');
            Route::post('edit_items','ItemsController@update');
            Route::delete('delete_items','ItemsController@destroy');
        });
        
        Route::resource('categories','CategoryController');
    });
    /*
    |==========================================|
    |                                          |
    |       Route groups for front             |
    |                                          |
    |==========================================|
    */
    Route::group(['namespace'=>'front'],function()
    {
        Route::get('/','FrontController@index');
        Route::get('/{slug1}/{slug2?}','FrontController@products')->where(['slug2' => '[a-z -]+', 'slug2' => '[a-z -]+']);
        Route::get('autocomplete', 'SearchAutoCompleteController@autocomplete')->name('autocomplete');
       
        // Route::get('/products/show',function(){
        //     return view('front.products');
        // });
        // Route::get('/{maincategories}/{categories}', function () {
        //     echo '1weerqwqwe';
        // })->where('categories', '[a-z]+');
    });
    Route::post('newsletter','NewsletterController@create');
    Route::post('newsletter','NewsletterController@store')->name('users.subscribe');
    /*
    |==========================================|
    |                                          |
    |        Route groups for users            |
    |                                          |
    |==========================================|
    */
    Route::group(['prefix'=>'users','namespace' => 'users','middleware'=>'user'], function ()
    {
        Route::get('/',['as'=>'users','uses'=>'UserController@index']);
        Route::get('/myshopping_cart','UserController@shoppingCart')->name('myshopping-cart');
        Route::get('/checkout','UserController@checkout');
        Route::get('/my_account','UserController@myAccount');
        Route::get('/my_wishlist','UserController@myAccount');
    });
    // Route::middleware('jwt.auth')->get('api_users', function(Request $request) {
    //     return auth()->user();
    // });
    
    
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
