<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace'=>'ApiAuth','middleware'=>'jwt.auth'], function()
{
    Route::get('user_login', 'ApiLoginController@login');
    Route::post('register', 'ApiLoginController@register');
});
// Route::get('/user', function () {
//     return response()->json('i am working');
// });
// Route::middleware('auth:api')->get('/users', function (Request $request) {
//     return $request->user();
// });
