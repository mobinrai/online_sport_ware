<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'user_name'         => 'milan',
            'email'             => 'milanrai@gmail.com',
            'email_verified_at' => now(),
            'password'          => Hash::make('nepal123'),
            'remember_token'    => Str::random(10),
            'created_at'        => now(),
            'is_active'         =>'1',
            'updated_at'        => now()

        ]);
    }
}
