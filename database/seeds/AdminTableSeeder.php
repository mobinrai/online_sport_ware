<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'admin_name'        => 'rphpdeveloper',
            'admin_email'       => 'mobinraee@gmail.com',
            'email_verified_at' => now(),      
            'password'          => Hash::make('123456'),
            'is_admin'          =>'1',  
            'is_active'         =>'1',  
            'remember_token'    => Str::random(10),
            'created_at'        => now(),
            'updated_at'        => now()

        ]);
    }
}
