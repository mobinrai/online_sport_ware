<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categories')->insert([[
            'name'      => strtolower('Men\'s Ware'),
            'parent_id' => '0',
            'slug'      => Str::slug('Men\'s Ware','-'),
        ],[
            'name'      => strtolower('Women\'s Ware'),
            'parent_id' => '0',
            'slug'      => Str::slug('Women\'s Ware','-'),
        ],[
            'name'      => strtolower('Men\'s Jacket'),
            'parent_id' => '1',
            'slug'      => Str::slug('Men\'s Jacket','-'),
        ],[
            'name'      => strtolower('Men\'s Shirt'),
            'parent_id' => '1',
            'slug'      => Str::slug('Men\'s Shirt','-'),
        ],[
            'name'      => strtolower('Men\'s Leather Jacket '),
            'parent_id' => '3',
            'slug'      => Str::slug('Men\'s Leather Jacket','-'),
        ],[
            'name'      => strtolower('Men\'s Normal Jacket '),
            'parent_id' => '3',
            'slug'      => Str::slug('Men\'s Normal Jacket','-'),
            
        ]]);
    }
}
