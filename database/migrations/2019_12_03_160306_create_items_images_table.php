<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('main_image_path');
            $table->string('front_image_path')->nullable();
            $table->string('back_image_path')->nullable();
            $table->string('side_image_path')->nullable();
            $table->bigInteger('item_id')->unsigned();
            $table->boolean('is_active');
            $table->foreign('item_id')->references('item_id')->on('items')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items_images');
    }
}
