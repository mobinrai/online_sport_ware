function Front(options)
{
    var opts = options;
    var elements= {
        btnNewletter   : $('.btn-newletter'),
        formNewLetter  : $('#formNewLetter'),
        newsLetterEmail: $('#news_letter_email'),
        submitModal : $('#submitModal'),
    }
    Front.prototype.init=function(){
        elements.btnNewletter.on('click',function(){
            if((elements.newsLetterEmail).data('bv-notempty') && (elements.newsLetterEmail).val()==='')
            {
                return false;
            }
            ajaxPost(elements.newsLetterEmail, opts.newsLetterUrl, function(successMessage)
                {
                    if(successMessage.success)
                    {
                        (elements.succcessModal).modal('show');
                        (elements.succcessModal).find('.modal-body').html(successMessage.msg);
                        (elements.succcessModal).on('hidden.bs.modal',function()
                        {
                            (elements.newsLetterEmail).val('');
                            
                        });
                    }
                    // else
                    // {
                    //     (elements.errorModal).modal('show');
                    //     (elements.erroModal).find('.modal-body').html(successMessage.msg);
                    //     (elements.submit_button).prop('disabled',false);
                    //     return false;
                    // }
                });
        });  
    }
}