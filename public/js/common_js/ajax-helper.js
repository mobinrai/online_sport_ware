function ajaxPost(formName, url,postData='', callback='')
{
	//BeforeSerialize(formName);
    var formData= formName.serialize()?formName.serialize():postData;
    var urlName = url ? url : formName.attr('action');
    $.ajax({
        type: 'POST',
        data: formData,
        url: urlName,
        cache: false,
        success:function(data)
        {
            if(data.success===true)
            {
                if(callback!=='')
                {
                    callback(data);
                }
                else
                {
                    $('#successModal').modal('show'); 
                    $('#successModal .modal-body').html(data.msg);
                }
            }
            else{
                $('#errorModal').modal('show'); 
                $('#errorModal .modal-body').html(data.msg);
                return false;
            }
           
        },
        error:function(jqXHR)
        {
            console.log(jqXHR.status);
            var responseMsg = jQuery.parseJSON(jqXHR.responseText);
            var errorsHtml='';
            console.log(responseMsg.errors);
            $.each(responseMsg.errors, function (index, value) 
            {
                errorsHtml += '<ul class="list-group"><li class="list-group-item alert alert-danger">' + value + '</li></ul>';
            });
            $('#errorModal').modal('show');            
            if(errorsHtml!=="")
            {
                $('#errorModal .modal-body').html(errorsHtml);
            }
            else
            {
                $('#errorModal .modal-body').html('Something went wrong.Try again later.');
            }
        }
    });
}

var ajaxGet = function (url, onBefore, onAfter) 
{
    $.ajax({
        type: 'GET',
        url: url,
        cache: false,
        beforeSend: function ()
        {
            if (onBefore)
            {
                onBefore();
            }
        }
    }).done(function (data)
    {
        if (onAfter)
        {
            onAfter(data);
        }
    });
};

