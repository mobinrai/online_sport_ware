function checkValidation(form_name)
{
    var hasError = false;
    $(form_name).find(':input[type="text"],:input[type="file"],select').each( function()
    {
        var requiredValue = $(this).data('bv-notempty');
        if(requiredValue=="true" && $(this).val()==='')
        {
            $(this).parent().find('.error_message').removeClass('hidden');
            $(this).css('border-color','red');
            hasError=true;
        }
    });
    return hasError;
}
function fileChange(e,className,previewId='')
{
    var ext      = event.target.files[0].type.split('/')[1];
    var extArray = ['png', 'jpeg']
    if(extArray.indexOf(ext) === -1)
    {
        event.target.value = '';
        $(e).parent().find('.'+className).removeClass('hidden');
        return false;
    }
    if(previewId!=='')
    {
        $('#'+previewId).attr('src',URL.createObjectURL(event.target.files[0]));
    }
    return true;
}
function hideErrorMessage ()
{
    $("[data-bv-notempty='true']").each(function()
    {
        
        $(this).on('focus',function()
        {
            var color=$(this).css("border-color");
            if(!$(this).parent().find('.error_message').hasClass('hidden'))
            {
                $(this).parent().find('.error_message').addClass('hidden')
            }
            if(!$(this).parent().find('.error_message1').hasClass('hidden'))
            {
                $(this).parent().find('.error_message1').addClass('hidden')
            }
            
            $(this).css('border-color','#d1d3e2');
        });
            
    });
}
var checkNumbers=function(e)
{
    var charCode = (e.which) ? e.which : e.keyCode
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}
