function Items(options)
{
    var opt     = options;
    var elements= {
            addForm     : $('#addItems'),

            succcessModal: $('#successModal'),
            submitModal : $('#submitModal'),
            errorModal  : $('#errorModal'),            
            
            submit_button: $('#add_item_submit'),
            itemName    : $('#item_name'),
            itemPrice   : $('#item_price'),
            itemQuantity: $('#item_quantity'),
            itemImage   : $('#image'),
            top_categories_list: $('#top_categories_list'),
            sub_category: $('#sub_category'),
        };
    Items.prototype.init = function()
    {
        hideErrorMessage();
        (elements.itemQuantity).on('keypress',function(e)
        {
            var value=$(this).val();
            if(value.length>5)
            {
                return false;
            }
            return checkNumbers(e);
        });
        (elements.itemPrice).on('keypress',function(){

        });
        (elements.top_categories_list).on('change',function()
        {
            var selectedValue=$(this).find("option:selected").val();
            if(selectedValue!=='')
            {
                // ajaxPost(elements.addForm, null, function(successMessage)
                // {
                //     if(successMessage.success)
                //     {

                //     }else{

                //     }
                // });
            }
        });
        (elements.itemImage).on('change',function(e)
        {
            var checkIsValid = fileChange(elements.itemImage,'item_image_type_error','image_preview');
            if(checkIsValid===false)
            {
                (elements.submit_button).prop('disabled',true);
            }else{
                (elements.submit_button).prop('disable',false);
            }
        });
       
        (elements.itemName).on('change',function()
        {
            var value   = $(this).val();
            var reg     = /^[a-z A-Z]+$/;
            if(!value.match(reg))
            {
                $(this).parent().find('.item_name_error').removeClass('hidden');
                return false;
            }
        });
        
        (elements.submit_button).on('click',function()
        {
            var hasError = checkValidation(elements.addForm);
            if(!hasError)
            {
                ajaxPost(elements.addForm, options.addItemsUrl, function(successMessage)
                {
                    if(successMessage.success)
                    {
                        (elements.succcessModal).modal('show');
                        (elements.succcessModal).find('.modal-body').html(successMessage.msg);
                        (elements.succcessModal).on('hidden.bs.modal',function()
                        {
                            (elements.itemName).val('');
                            (elements.itemPrice).val('');
                            (elements.itemQuantity).val('');
                            (elements.top_category).find('option[value=""]').attr("selected",true);
                            (elements.sub_category).find('option[value=""]').attr("selected",true);
                            (elements.submit_button).prop('disabled',false);
                        });
                    }
                    // else
                    // {
                    //     (elements.errorModal).modal('show');
                    //     (elements.erroModal).find('.modal-body').html(successMessage.msg);
                    //     (elements.submit_button).prop('disabled',false);
                    //     return false;
                    // }
                });
            }
            else{
                return false;
            }
            
        });        
    };
}

function ItemsEditDelete(options)
{
    var opts    = options;
    var elements= {
        editSubCategoryForm : $('.theForm'),
        topCategoryList     : $('.top_categories_list'),
        editsubCategoryName : $('#name'),
        editSubmitModal     : $('.btn-edit-subcategories'),
        errorModal          : $('#errorModal'),
        successModal        : $('#successModal'),
        deleteSubCategoryBtn: $('.btn-deleteSubCategory'),
        editModal           : $('.editModal'),
        topCategoryValue    : $('.top_category_value'),
        editSubmitBtn       : $('.edit_submit_btn')
    }
}