function SubCategoriesAdd(options)
{
    var opts    = options;
    var elements= {
        addSubCategoryForm  : $('#subcategories'),
        topCategoryList     : $('#top_categories_list'),
        subCategoryName     : $('#name'),
        submitBtn           : $('.btn-add-subcategories'),
        errorModal          : $('#errorModal'),
        successModal        : $('#successModal')
    };
    SubCategoriesAdd.prototype.init=function()
    {
        hideErrorMessage();
        
        (elements.subCategoryName).on('change',function()
        {
            var value   = $(this).val();
            var reg     = /^[a-z A-Z \']+$/;
            if(value !=='' && !value.match(reg))
            {
                $(this).val('');
                $(this).parent().find('.sub_category_validation_error').removeClass('hidden');
                (elements.submitBtn).prop('disabled',true);
                return false;
            }            
        });
        (elements.submitBtn).on('click',function(e){
           e.preventDefault();
            var isError = checkValidation(elements.addSubCategoryForm);
            if(!isError)
            {
                ajaxPost(elements.addSubCategoryForm,opts.addSubCategoryUrl,'',function(addSubCategory){
                    if(addSubCategory.success)
                    {
                        (elements.successModal).modal('show');
                        (elements.successModal).find('.modal-body').html(addSubCategory.msg);
                        (elements.successModal).on('hidden.bs.modal', function () {
                            (elements.subCategoryName).val('');
                            (elements.topCategoryList).find('option[value=""]').attr("selected",true);
                            (elements.submitBtn).prop('disabled',false);
                        });
                        
                    }
                    // else
                    // {
                    //     (elements.errorModal).modal('show');
                    //     (elements.errorModal).find('.modal-body').html(addSubCategory.msg);
                    //     (elements.submitBtn).prop('disabled',false);
                    //     return false;
                    // }
                });
            }
            (elements.submitBtn).prop('disabled',true);
            return !isError;
        });
    };            
}
function SubCategoryEditDelete(options)
{
    var opts=options;
    var elements= {
        editSubCategoryForm : $('.theForm'),
        topCategoryList     : $('.top_categories_list'),
        editsubCategoryName : $('#name'),
        editSubmitModal     : $('.btn-edit-subcategories'),
        errorModal          : $('#errorModal'),
        successModal        : $('#successModal'),
        deleteSubCategoryBtn: $('.btn-deleteSubCategory'),
        editModal           : $('.editModal'),
        topCategoryValue    : $('.top_category_value'),
        editSubmitBtn       : $('.edit_submit_btn')
    }; 

    SubCategoryEditDelete.prototype.init=function()
    {
        
        (elements.editSubmitModal).on('click',function()
        {
            $(this).parent('.table_td').find(elements.editModal).modal('show');  
        });
        (elements.editSubmitBtn).on('click',function(e)
        {
            var form    = $(this).closest('tr').find(elements.editSubCategoryForm);
            var isError = checkValidation(form);
            if(!isError)
            {
                (elements.editModal).modal('hide');
                ajaxPost(form,opts.editUrl,'',function(editSubCategory){
                    if(editSubCategory.success)
                    {
                        (elements.successModal).modal('show');
                        (elements.successModal).find('.modal-body').html(editSubCategory.msg);
                        (elements.successModal).on('hidden.bs.modal', function () {
                            (elements.subCategoryName).val('');
                            (elements.topCategoryList).find('option[value=""]').attr("selected",true);
                            (elements.submitBtn).prop('disabled',false);
                        });
                    }
                    // else
                    // {
                    //     (elements.errorModal).modal('show');
                    //     (elements.errorModal).find('.modal-body').html(editSubCategory.msg);
                    //     (elements.submitBtn).prop('disabled',false);
                    //     return false;
                    // }
                });
            }
        });        
    }
}

