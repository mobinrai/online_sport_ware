function TopCategories(options)
{
    var opts = options;
    var elements= {
                addForm             : $('#addTopcategories'),
                top_category_name   : $('#name'),
                addTopcategoriesBtn : $('.btn-add-categories'),
                errorModal          : $('#errorModal'),
                successModal        : $('#successModal')
            };
    TopCategories.prototype.init=function()
    {
        //checkValidation();
        WireUpActions();
        hideErrorMessage();
        (elements.top_category_name).on('change',function()
        {
            var value   = $(this).val();
            var reg     = /^[a-z A-Z\']+$/;
            if(value!=='' && !value.match(reg))
            {
                $(this).val('');
                $(this).parent().find('.invalid_name_error').removeClass('hidden');
                (elements.addTopcategoriesBtn).prop('disabled',true);
                $(this).css('border-color','red');
                return false;
            }
            else
            {
                (elements.addTopcategoriesBtn).prop('disabled',false);
            }
        });        
    };
    var WireUpActions=function()
    {
        (elements.addTopcategoriesBtn).click(function(e)
        {
            e.preventDefault();
            var isError = checkValidation(elements.addForm);
            if(!isError)
            {
                ajaxPost(elements.addForm,opts.addTopCategoiresUrl,'',function(addTopcategories)
                {
                    if(addTopcategories.success)
                    {
                        (elements.successModal).modal('show');
                        (elements.successModal).find('.modal-body').html(addTopcategories.msg);
                        (elements.successModal).on('hidden.bs.modal', function () {
                            (elements.top_category_name).val('');
                        });
                    }
                    // else if(addTopcategories.success===false)
                    // {
                    //     (elements.errorModal).modal('show');
                    //     (elements.errorModal).find('.modal-body').html(addTopcategories.msg);
                    // }
                });
            }
            else
            {
                (elements.addTopcategoriesBtn).prop('disabled',true);
                return false;
            }            
        });
    };    
}

function TopCategoryEditDelete(options)
{
    var opts=options;
    var elements={
        editModal       : $('.editModal'),
        editForm        : $('#theForm'),
        editName        : $('#name'),
        editButton      : $('.btn-edit'),
        editSubmitButton: $('.edit_submit_btn'),
        editDelete      : $('.editDelete'),
        errorModal      : $('#errorModal'),
        successModal    : $('#successModal')
    };
    TopCategoryEditDelete.prototype.init=function()
    {
        hideErrorMessage();
        (elements.editButton).on('click',function()
        {
            $('#top_cat_value').val($(this).data('bv-id'));
            $('#name').val($(this).data('bv-name'));
            (elements.editModal).find('.error_message').addClass('hidden');
            (elements.editModal).find('#name').css('border-color','#d1d3e2');
            (elements.editModal).modal('show');
            
        });
        
        (elements.editSubmitButton).on('click',function(e)
        {
            e.preventDefault();
            var isError = checkValidation(elements.editForm);
            if(!isError)
            {
                (elements.editModal).modal('hide');
                ajaxPost(elements.editForm,opts.editTopCategories,'',function(editTopcategories)
                {
                    if(editTopcategories.success)
                    {
                        (elements.successModal).modal('show');
                        (elements.successModal).find('.modal-body').html(editTopcategories.msg);
                        (elements.successModal).on('hidden.bs.modal', function () {
                            location.reload();
                        });
                    }
                    // else if(editTopcategories.success===false)
                    // {
                    //     (elements.errorModal).modal('show');
                    //     (elements.errorModal).find('.modal-body').html(editTopcategories.msg);
                    //     return false;
                    // }
                });
            }
        });  
        (elements.editDelete).on('click',function(){
            var id=$(this).data('id');
            $('#myGlobalModal').modal();
            $('#myGlobalModal .modal-title').html('Confirm Message');
            $('#myGlobalModal .modal-body').html('Are you Sure you want to delete?');
            $('#modal-submit-btn').click(function(e)
            {

                $('#myGlobalModal').modal('hide');
                var url=opts.deleteTopCategories;
                e.preventDefault();
                $.ajax({
                    url     : url+'/'+id,
                    async   : false,
                    dataType: 'json',
                    type    : 'delete',
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data    : { "_token":opts.csrf_token, 'cat_id' : id},
                    success:function(data)
                    {
                        if(data.success===true)
                        {
                            (elements.successModal).modal('show');
                            (elements.successModal).find('.modal-body').html(data.msg);
                            (elements.successModal).on('hidden.bs.modal', function () {
                                location.reload();
                            });
                        }
                        // else if(data.success===false)
                        // {
                        //     (elements.errorModal).modal('show');
                        //     (elements.errorModal).find('.modal-body').html(data.msg);
                        //     return false;
                        // }
                    },
                    error:function(data)
                    {
                        (elements.errorModal).modal('show');
                        (elements.errorModal).find('.modal-body').html('Someting went wrong');
                        return false;
                    },
                    complete:function()
                    {
                        //console.log('i am ok');
                    }
                });
            });
        });
        
    }
}

