function Categories(options)
{
    var opts=options;
    var elements={
        categoryType:$('#category_type'),
        categoryParentName:$('#category_parent_name'),
        parentList: $('#parent_list'),
        submitBtn:$('.btn-add-categories'),
        addForm:$('#addCategories'),
        errorModal      : $('#errorModal'),
        successModal    : $('#successModal'),
        slug:$("#slug"),
        name:$("#name"),
    };

    Categories.prototype.init=function()
    {
        hideErrorMessage ();
        (elements.name).on('keypress',function(){
            var regex = new RegExp("^[a-zA-Z ]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
            return true;
        });
        (elements.slug).on('keypress',function(){
            var regex = new RegExp("^[a-zA-Z-]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
            return true;
        });
        (elements.categoryType).on('change',function(){
            (elements.parentList).addClass('hidden');
            (elements.categoryParentName).attr('data-bv-notempty',false);
            if($(this).val()==='child'){
                (elements.parentList).removeClass('hidden');
                (elements.categoryParentName).attr('data-bv-notempty',true);
            }
        });
        (elements.submitBtn).click(function(e)
        {
            e.preventDefault();
            var isError = checkValidation(elements.addForm);
            if(!isError)
            {
                ajaxPost(elements.addForm,opts.addCategory,'',function(successMessage){
                    if(successMessage.success===true){
                        (elements.successModal).modal('show');
                        (elements.successModal).find('.modal-body').html(successMessage.msg);
                        (elements.successModal).on('hidden.bs.modal', function () {                            
                            (elements.slug).val("");
                            (elements.name).val("");
                            (elements.parentList).addClass('hidden');
                            (elements.categoryType).find('option[value="parent"]').attr("selected",true);
                            (elements.categoryParentName).attr('data-bv-notempty',false);
                        });
                    }
                })
            }
        });
    }    
}


function CategoryEdit(opt){
    var options=opt;
    var elements={
        editButton:$('.btn-edit'),
        editForm:$("#editForm"),
        slug:$("#slug"),
        name:$("#name"),
        editModal:$('.editModal'),
        category:$('#category_id'),
        editSubmitButton:$('#edit_submit_btn'),
        deleteBtn:$('.editDelete'),
        errorModal      : $('#errorModal'),
        successModal    : $('#successModal')
    }
    CategoryEdit.prototype.init=function()
    {
        hideErrorMessage();
        (elements.name).on('keypress',function(){
            var regex = new RegExp("^[a-zA-Z ]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
            return true;
        });
        (elements.slug).on('keypress',function(){
            var regex = new RegExp("^[a-zA-Z-]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
            return true;
        });
        (elements.editButton).on('click',function(){
            (elements.slug).val($(this).attr('data-bv-slug'));
            (elements.name).val($(this).attr('data-bv-name'));
            (elements.category).val($(this).attr('data-bv-id'));
            (elements.editModal).modal('show');
        });
        (elements.editSubmitButton).on('click',function(e){
            e.preventDefault();
            var isError = checkValidation(elements.editForm);
            var id=elements.category.val();
            if(!isError)
            {
                ajaxPost(elements.editForm,(opt.update+'/'+id),'',function(editStatus){
                    alert(editStatus.success)
                });
            }
        });
        (elements.deleteBtn).on('click',function(e){
            //data-bv-id
            e.preventDefault();
            var id=$(this).data('bv-id');
            var urlDestory=options.delete+'/'+id;
            $('#myGlobalModal').modal();
            $('#myGlobalModal .modal-title').html('Confirm Message');
            $('#myGlobalModal .modal-body').html('Are you Sure you want to delete?And All its Related List.');
            $('#modal-submit-btn').click(function(e)
            {
                $('#myGlobalModal').modal('hide');
                $.ajax({
                    url     : urlDestory,
                    async   : false,
                    type    : 'POST',
                    headers : { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data    : { "_token":options.csrf_token, _method: 'DELETE'},
                    success:function(data)
                    {
                        console.log(data);
                        if(data.success===true)
                        {
                            (elements.successModal).modal('show');
                            (elements.successModal).find('.modal-body').html(data.msg);
                            (elements.successModal).on('hidden.bs.modal', function () {
                                location.reload();
                            });
                        }
                        
                    },
                    error:function(data)
                    {
                        (elements.errorModal).modal('show');
                        (elements.errorModal).find('.modal-body').html('Someting went wrong');
                        return false;
                    },
                    complete:function()
                    {
                        //console.log('i am ok');
                    }
                });
                });
        });
    }
}
