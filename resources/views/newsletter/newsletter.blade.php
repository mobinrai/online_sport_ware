<form method="POST" action="" id="formNewLetter">
    @csrf

    <div class="row inner-form-group">
        <div class="col-md-12">
            <label for="Email">Email:</label>
            <input type="text" class="form-control" id="news_letter_email" name="email" data-bv-notempty="true">
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-4">
            <button class="btn btn-danger btn-newletter">Submit</button>
        </div>
        <div class="col-md-4"></div>
    </div> 
</form>