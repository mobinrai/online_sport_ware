@extends('admin.layouts.app')
@section('content')
    <div id="content">
        <!--=================== Begin Page Content ===============-->
        <div class="container-fluid">
            <div class="row">
                <!--=================== Begin Card Content ===============-->
                <div class="card shadow no-border-radius col-xl-8 col-lg-7">
                    <div class="card-header py-3 d-flex flex-row align-items-center">
                        <h6 class="m-0 font-weight-bold card-header-text">Add Items</h6>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <form action="" method="POST" enctype="multipart/form-data" id="addItems">
                                @csrf
                                <div class="form-group">
                                    <div class="col-md-10">
                                    <div class="inner-form-group">
                                    <label class="add_items" for="top_name">{{'Top Category'}}</label><span class='text-notes'>(Notes:Please select atleast one Top Category)</span><br>
                                        <select name="top_categories_list" id="top_categories_list" style="font-size:14px" data-bv-notempty="true">
                                            <option value="">--Select Top Category--</option>
                                            @foreach ($top_categories as $top_category)
                                                <option value="{{ Hashids::connection('topCategories')->encode($top_category->id) }}">{{ ucfirst($top_category->name)}}</option>
                                            @endforeach
                                        </select>
                                        <br>
                                        <span class='item_top_category error_message hidden'>Top Category is required.</span>
                                    </div>
                                    <div class="inner-form-group">
                                    <label class="add_items" for="top_name">{{'Sub Category'}}</label><span class='text-notes'>(Notes:Please select atleast one Sub Category)</span><br>
                                        <select name="top_categories_list" id="top_categories_list" style="font-size:14px" data-bv-notempty="true">
                                            <option value="">--Select Top Category--</option>
                                        </select>
                                        <br>
                                        <span class='item_top_category error_message hidden'>Sub Category is required.</span>
                                    </div>
                                    <div class="inner-form-group">
                                        <label class="add_items" for="item_name">Items</label><span class='text-notes'>(Notes:Please enter alphabets and ' only)</span>
                                        <input type="text" name="item_name" id="item_name" value="" data-bv-notempty="true" data-bv-notempty-message="Item Name is required." class="form-control" data-bv-field="item_name">
                                        <span class='item_name_required_error error_message hidden'>Items name is required.</span>
                                        <span class='item_name_error hidden'>Item name is not valid.</span>
                                    </div>
                                    <div  class="inner-form-group">
                                        <label class="add_items" for="item_price">Price</label><span class='text-notes'>(Notes:Please enter numbers with single dot(.) only)</span>
                                        <input type="text" name="item_price" id="item_price" value="" data-bv-notempty="true" data-bv-notempty-message="Item Price is required." class="form-control" data-bv-field="item_price">
                                        <span class='item_price_error error_message hidden'>Items Price is required.</span>
                                    </div>
                                    <div  class="inner-form-group">
                                        <label class="add_items" for="item_quantity">Quantity</label><span class='text-notes'>(Notes:Please enter numbers only)</span>
                                        <input type="text" name="item_quantity" id="item_quantity" value="" data-bv-notempty="true" class="form-control"  data-bv-field="item_quantity">
                                        <span class='item_quantity_error error_message hidden'>Quantity is required.</span>
                                    </div>
                                    <div class="inner-form-group">
                                        <label for="image" class="col-form-label text-md-right">{{ __('Image') }}</label><span class='text-notes'>(Notes:Please enter images with extension .jpg and .png)</span>
                                        <br>
                                        <img id="image_preview" class="image-preview" src="{{ url('/images/sites_image/no-preview-available.png')}}" alt="Image Preview">
                                        <br>
                                        <input id="image" type="file" class="image" name="image" value="{{ old('image') }}" data-bv-notempty="true">
                                        <br>
                                        <span class='item_image_type_error error_message1 hidden'>Image must be .jpg and .png</span>
                                        <span class='item_image_error error_message hidden'>Image is required.</span>
                                    </div>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <button  type="button" id="add_item_submit"  class="btn btn-login btn-primary" style="width: 150px; padding: 5px 0px;">
                                            {{ __('Add') }}
                                        </button>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--=================== End Card Content ===============-->
            </div>
            <!--=================== End Row ===============-->
        </div>
        <!--=================== End Container Fluid ===============-->
    </div>
    {{--  error: function (request, status, error) {
        json = $.parseJSON(request.responseText);
        $.each(json.errors, function(key, value){  --}}
    @section('scripts')
    <script src="{{ asset('js/admin_js/items.js')}}"></script>
        <script>
            $(document).ready(function()
            {
                var options=
                {
                    addItemsUrl     : '{{ url("admin/items/add_items") }}',
                    getSubGategories: '{{ url("admin/top_categories/get_subcategories_items") }}'
                }
                var items=new Items(options);
                items.init();
            });
            
        </script>
    @endsection
@endsection
