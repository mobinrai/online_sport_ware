@extends('admin.layouts.app')
@section('content')
    <div id="content">
        <!--=================== Begin Page Content ===============-->
        <div class="container-fluid">
            <div class="card bg-light shadow col-md-9">
                <div class="card-header py-3 text-center">
                    <h6 class="m-0 font-weight-bold card-header-text">{{ 'Top Categories' }}</h6>
                </div>
                <div class="card-body top_categories">
                    <table class="width-100per categories-tbl table-stripe">
                        <thead>
                            <th>S.N</th>
                            <th>Name</th>
                            <th>Sub Categories Count</th>
                            <th>Action</th>
                        </thead>
                        @foreach ($top_categories as $categories)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ ucfirst($categories->name) }}</td>
                                <td>{{ $categories->subCategories->count() }}</td>
                                <td class="table_td">
                                    <button  type="button"  class="btn btn-primary btn-edit" id="btn-edit" data-bv-name="{{ ucfirst($categories->name) }}" data-bv-id='{{ Hashids::connection('topCategories')->encode($categories->id)}}' style="width: 150px; padding: 5px 0px;">
                                        {{ __('Edit') }}
                                    </button>
                                    <button  type="button"  class="btn btn-red btn-delete editDelete" id="editDelete" data-id='{{ Hashids::connection('topCategories')->encode($categories->id)}}' style="width: 150px; padding: 5px 0px;">
                                        {{ __('Delete') }}
                                    </button>
                                </td>
                            </tr>
                        @endforeach

                        <div class="modal editModal" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel">
                            <div class="modal-dialog" role="document" style="margin-top: 80px;">
                                <div class="modal-content no-border-radius">
                                    <div class="modal-header custom-header no-border-radius">
                                        <h5 class="modal-title" id="editModalLabel">Edit Top Categories</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    </div>
                                    <div class="modal-body">
                                        <form id="theForm" action="" method="POST">
                                            @csrf
                                            <input type="hidden" name="id" id="top_cat_value" value="">
                                            <div class="form-group row">
                                                <label class="reg change_label_display" for="name">Name :</label>
                                                <input type="text" name="name" id="name" data-bv-notempty="true" class="change_input_display form-control" value="Top Category" style="width: 230px">
                                                <br>
                                                <span class='top_category_name_error error_message hidden'>Top Category Name is required.</span>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer" style="height: 50px;">
                                            <input type="button" id="edit_submit_btn" class="btn btn-primary modal-submit-btn edit_submit_btn" value="Submit">
                                        <button type="button" class="btn btn-primary no-border-radius modal-close-btn" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </table>
                </div>
                <div class="card-footer text-muted">
                </div>
            </div>
        </div>
    </div>
    @section('scripts')
    <script src="{{ asset('js/admin_js/topCategories.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var options ={
                editTopCategories:'{{ url("admin/top_categories/edit") }}',
                deleteTopCategories:'{{ url("admin/top_categories/delete") }}',
                csrf_token:'{{ csrf_token() }}'
            };
            var topCategoryEditDelete = new TopCategoryEditDelete(options);
            topCategoryEditDelete.init();
        });        
    </script>
    @endsection
@endsection
