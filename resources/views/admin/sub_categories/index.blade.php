@extends('admin.layouts.app')
@section('content')
<div id="content">
    <!--=================== Begin Page Content ===============-->
    <div class="container-fluid">
        <div class="card bg-light shadow col-md-9">
            <div class="card-header py-3 text-center">
                <h6 class="m-0 font-weight-bold card-header-text">{{ 'Sub Categories' }}</h6>
            </div>
            <div class="card-body image-slider">
                <table class="width-100per subCategories_tbl table-stripe">
                    <thead>
                        <th>S.N</th>
                        <th>Name</th>
                        <th>Top Category Name</th>
                        <th>Action</th>
                    </thead>
                    @foreach ($sub_categories as $categories)
                        <tr class="table_tr">
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ ucfirst($categories->name) }}</td>
                            <td>{{ ucfirst($categories->topCategories->name) }}</td>
                            <td class="table_td">
                                <input type="submit" class="btn btn-primary btn-edit btn-edit-subcategories" value="Edit">
                                <input type="button" class="btn btn-red btn-edit btn-deleteSubCategory" id="deleteSubCategory"  data-bv-value='{{ Hashids::connection('subCategories')->encode($categories->id)}}' value="Delete">
                                <div class="modal editModal" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel">
                                    <div class="modal-dialog" role="document" style="margin-top: 80px;">
                                        <div class="modal-content no-border-radius">
                                            <div class="modal-header custom-header no-border-radius">
                                                <h5 class="modal-title" id="editModalLabel">Edit Sub Categories</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="col-md-12">
                                                    <form id="theForm" class="theForm" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{ Hashids::connection('subCategories')->encode($categories->sub_category_id)}}">
                                                        
                                                        <div class="form-group">
                                                            <div class="col-md-12 inner-form-group">
                                                                <label class="reg" for="name">Top Categories : </label><span class='text-notes'>(Notes:Please select atleast one top category)</span><br>
                                                                <select data-bv-notempty="true" class="top_categories_list" name="top_categories_list">
                                                                    <option value="">--Select Top Category--</option>
                                                                    @foreach ($top_categories as $top_category)
                                                                    <?php $selected='';?>
                                                                    @if($top_category->id==$categories->topCategories->id)
                                                                        <?php $selected='selected';?>
                                                                    @endif
                                                                        <option value="{{ Hashids::connection('topCategories')->encode($top_category->top_category_id) }}" {{$selected}}>{{ ucfirst($top_category->name)}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>                                                               
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-md-12 inner-form-group">    
                                                                <label class="reg" for="name">Name :</label><span class='text-notes'>(Notes:Please enter alphabets and ' only)</span><br>
                                                                <input type="text" name="name" id="name" value="{{  ucfirst($categories->name)}}">
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>       
                                            </div>
                                            <div class="modal-footer" style="height: 50px;">
                                                <input type="button" class="btn btn-primary modal-submit-btn edit_submit_btn" value="Submit">
                                                <button type="button" class="btn btn-primary no-border-radius modal-close-btn" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="card-footer text-muted">
            </div>
        </div>
    </div>
</div>
@section('scripts')
<script src="{{ asset('js/admin_js/subCategories.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var options={
            editUrl:"{{ url('admin/sub_categories/edit') }}",
            deleteUrl:"{{ url('/admin/sub_categories/delete') }}"
        };
        var subCategoriesEditDelete=new SubCategoryEditDelete(options);
        subCategoriesEditDelete.init();
    });
    function editTopCategories(selected)
    {
        
    }
    function deleteTopCategory(selected)
    {
        var id=$(selected).data('id');
        $('#myGlobalModal').modal();
        $('#myGlobalModal .modal-title').html('Confirm Message');
        $('#myGlobalModal .modal-body').html('Are you Sure you want to delete?');
        //$('#theForm').submit();
        $('#modal-submit-btn').click(function(e)
        {
            $('#myGlobalModal').modal('hide');
            var url="";
            e.preventDefault();
            //$('#theForm').submit();
            $.ajax({
                url     : url+'/'+id,
                dataType: 'json',
                type    : 'delete',
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data    : { "_token":`{{ csrf_token() }}`, 'sub_cat_id' : id},
                success:function(data)
                {
                    if(data.success===true)
                    {
                        location.reload();
                        console.log(data);
                    }
                    else if(data.success===false)
                    {
                        $('#myGlobalModal').modal();
                        $('#myGlobalModal .modal-title').html('Error Message');
                        $('#myGlobalModal .modal-body').html('Couldnot delete Top Categories..');
                    }
                },
                error:function(data)
                {
                    $('#myGlobalModal .modal-title').html('Error Message');
                    $('#myGlobalModal .modal-body').html('Couldnot delete Top Categories..');
                },
                complete:function()
                {
                    //console.log('i am ok');
                }
            });
        });
    }
</script>
@endsection
@endsection
