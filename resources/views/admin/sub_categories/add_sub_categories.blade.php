@extends('admin.layouts.app')
@section('content')
    <div id="content">
        <!--=================== Begin Page Content ===============-->
        <div class="container-fluid">
            <div class="row">
                <!--=================== Begin Card Content ===============-->
                <div class="card shadow no-border-radius col-xl-8 col-lg-7">
                    <div class="card-header py-3 d-flex flex-row align-items-center">
                        <h6 class="m-0 font-weight-bold card-header-text">Add Sub Categories</h6>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <!-- ==================== Sub Categories form ======================-->
                            <form method="POST" id="subcategories">
                                @csrf
                                <div class="form-group">
                                    <div class="col-md-10 inner-form-group">
                                        <label class="reg" for="name">Top Categories</label><span class='text-notes'>(Notes:Please select atleast one top category)</span><br>
                                        <!-- ==================== Top Categories list ======================-->
                                        <select name="top_categories_list" id="top_categories_list" style="font-size:14px" data-bv-notempty="true" >
                                            <option value="">--Select Top Category--</option>
                                            @foreach ($top_categories as $top_category)
                                                <option value="{{Hashids::connection('topCategories')->encode($top_category->top_category_id)}}">{{ ucfirst($top_category->name)}}</option>
                                            @endforeach
                                        </select>
                                        <i class="form-control-feedback glyphicon-asterisk glyphicon glyphicon-ok" data-bv-icon-for="name" style="display: block;"></i>
                                        <span class='top_category_require_error error_message hidden'>Top Category name is required.</span>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-10 inner-form-group">
                                        <label class="reg" for="name">Sub Categories</label><span class='text-notes'>(Notes:Please enter alphabets and ' only)</span>
                                        <input type="text" name="name" id="name" value="" data-bv-notempty="true" data-bv-notempty-message="User name is required." class="form-control" data-bv-field="name">
                                        <i class="form-control-feedback glyphicon-asterisk glyphicon glyphicon-ok" data-bv-icon-for="name" style="display: block;"></i>
                                        <span class='sub_category_require_error error_message hidden'>Sub Category name is required.</span>
                                        <span class='sub_category_validation_error error_message1 hidden'>Sub Category name is not valid.</span>
                                        @if($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <button  type="button" class="btn btn-add-subcategories btn-primary" style="width: 150px; padding: 5px 0px;">
                                            {{ __('Add') }}
                                        </button>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </form>
                            <!-- ==================== End Sub Categories form ======================-->
                        </div>
                    </div>
                </div>
                <!--=================== End Card Content ===============-->
            </div>
            <!--=================== End Row ===============-->
        </div>
        <!--=================== End Container Fluid ===============-->
    </div>
    {{--  error: function (request, status, error) {
        json = $.parseJSON(request.responseText);
        $.each(json.errors, function(key, value){  --}}
    @section('scripts')
    <script src="{{ asset('js/admin_js/subCategories.js')}}"></script>
        <script>
            $(document).ready(function()
            {
                var options={
                    addSubCategoryUrl:"{{ url('admin/sub_categories/add_sub_categories') }}"
                };
                var subCategoriesAdd=new SubCategoriesAdd(options);
                subCategoriesAdd.init();
            });
            
        </script>
    @endsection
@endsection
