@extends('admin.layouts.app')
@section('content')
    <div id="content">
        <!--=================== Begin Page Content ===============-->
        <div class="container-fluid">
            <div class="row">
                <!--=================== Begin Card Content ===============-->
                <div class="card shadow no-border-radius col-xl-8 col-lg-7">
                    <div class="card-header py-3 d-flex flex-row align-items-center">
                        <h6 class="m-0 font-weight-bold card-header-text">Add Top Categories</h6>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <form action="" method="POST" id="addTopcategories">
                                @csrf
                                <div class="form-group">
                                    <div class="col-md-10">
                                    <div class="inner-form-group">
                                        <label class="lable-text" for="name">Top Categories</label><span class='text-notes'>(Notes:Please enter alphabets and ' only)</span>
                                        <input type="text" name="name" id="name" value="" data-bv-notempty="true" data-bv-notempty-message="Top Category ame is required." class="form-control" data-bv-field="name">
                                        <span class='invalid_name_error error_message1 hidden'>Invalid Top Category name.</span>
                                        <span class='name_require_error error_message hidden'>Top Category name is required.</span>
                                    </div>
                                        
                                        <label class="lable-text" for="slug">Slug</label>
                                        <input type="text" name="slug" id="slug" value="" data-bv-notempty-message="Slug is required." class="form-control" data-bv-field="slug">
                                        <i class="form-control-feedback glyphicon-asterisk glyphicon glyphicon-ok" data-bv-icon-for="name" style="display: block;"></i>
                                        <small class="help-block" data-bv-validator="notEmpty" data-bv-for="name" data-bv-result="VALID" style="display: none;">Top Category Name is required.</small>
                                        @if($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                                <strong>{{ $errors->first('slug') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <button  type="button"  class="btn btn-add-categories btn-primary" style="width: 150px; padding: 5px 0px;">
                                            {{ __('Add') }}
                                        </button>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--=================== End Card Content ===============-->
            </div>
            <!--=================== End Row ===============-->
        </div>
        <!--=================== End Container Fluid ===============-->
    </div>
    {{--  error: function (request, status, error) {
        json = $.parseJSON(request.responseText);
        $.each(json.errors, function(key, value){  --}}
    @section('scripts')
    <script src="{{ asset('js/admin_js/topCategories.js')}}"></script>
    <script>
        $(document).ready(function()
        {
            var options={
                addTopCategoiresUrl:'{{ url("admin/top_categories/add_top_categories") }}'
            };
            var topCategories=new TopCategories(options);
            topCategories.init();
        });
    </script>
    @endsection
@endsection
