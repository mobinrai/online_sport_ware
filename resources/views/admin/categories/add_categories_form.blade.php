@extends('admin.layouts.app')
@section('content')
    <div id="content">
        <!--=================== Begin Page Content ===============-->
        <div class="container-fluid">
            <div class="row">
                <!--=================== Begin Card Content ===============-->
                <div class="card shadow no-border-radius col-xl-8 col-lg-7">
                    <div class="card-header py-3 d-flex flex-row align-items-center">
                        <h6 class="m-0 font-weight-bold card-header-text">Categories</h6>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <!-- ==================== Sub Categories form ======================-->
                            <form method="POST" id="addCategories">
                                @csrf
                                <div class="form-group">
                                    <div class="col-md-10 inner-form-group">
                                        <label class="reg change_label_display" for="name">Categories Type</label><span class='text-notes'>(Notes:Please select atleast Type)</span><br>
                                        <!-- ==================== Top Categories list ======================-->
                                        <select name="category_type" id="category_type" data-bv-notempty="false">
                                            <option value="parent">Parent</option>
                                            <option value="child">Child</option>
                                        </select>
                                        <i class="form-control-feedback glyphicon-asterisk glyphicon glyphicon-ok" data-bv-icon-for="name" style="display: block;"></i>
                                        <span class='top_category_require_error error_message hidden'>Type name is required.</span>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                                <div class="form-group hidden"  id="parent_list">
                                    <div class="col-md-10 inner-form-group">
                                        <label class="reg change_label_display" for="name">Parent Name</label><span class='text-notes'>(Notes:Please select one parent)</span><br>
                                        <select name="category_parent_name" id="category_parent_name" data-bv-notempty="true">
                                            <option value="">--Select Parent--</option>
                                            @foreach ($parentCategories as $parentCat)
                                                <option value="{{ Hashids::connection('categories')->encode($parentCat->id)}}">{{ ucfirst($parentCat->name)}}</option>
                                            @endforeach                                        
                                        </select>
                                        <br>
                                        <span class='category_parent_name_error error_message hidden'>Category name is required.</span>
                                        <span class='category_parent_name_error error_message1 hidden'>Category name is not valid.</span>
                                        
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-10 inner-form-group">
                                        <label class="reg change_label_display" for="name">Category Name</label><span class='text-notes'>(Notes:Please enter alphabets and ' only)</span>
                                        <input type="text" name="name" id="name" value="" data-bv-notempty="true" class="form-control" data-bv-field="name">
                                        <i class="form-control-feedback glyphicon-asterisk glyphicon glyphicon-ok" data-bv-icon-for="name" style="display: block;"></i>
                                        <span class='category_require_error error_message hidden'>Category name is required.</span>
                                        <span class='category_validation_error error_message1 hidden'>Category name is not valid.</span>
                                        @if($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-10 inner-form-group">
                                        <label class="slug change_label_display" for="name">Slug :<span class='text-notes'>(Notes:Please enter alphabets and - only)</span></label>
                                        <input type="text" name="slug" id="slug" data-bv-notempty="true" class="change_input_display form-control" style="width: 80%;">
                                        <br>
                                        <span class='category_slug_required_error error_message hidden' style="padding-left: 13px;">Slug is required.</span>
                                        <span class='category_slug_validation_error error_message hidden' style="padding-left: 13px;">Slug is not required.</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <button  type="button" class="btn btn-add-categories btn-primary" style="width: 150px; padding: 5px 0px;">
                                            {{ __('Add') }}
                                        </button>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </form>
                            <!-- ==================== End Sub Categories form ======================-->
                        </div>
                    </div>
                </div>
                <!--=================== End Card Content ===============-->
            </div>
            <!--=================== End Row ===============-->
        </div>
        <!--=================== End Container Fluid ===============-->
    </div>
    {{--  error: function (request, status, error) {
        json = $.parseJSON(request.responseText);
        $.each(json.errors, function(key, value){  --}}
    @section('scripts')
    <script src="{{ asset('js/admin_js/categories.js')}}"></script>
        <script>
            $(document).ready(function(){
                var options={
                    "addCategory":"{{ url('admin/categories') }}"
                }
                var categories=new Categories(options);
                categories.init();
            });     
        </script>
    @endsection
@endsection
