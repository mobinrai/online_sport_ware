@extends('admin.layouts.app')
@section('content')
    <div id="content">
        <!--=================== Begin Page Content ===============-->
        <div class="container-fluid">
            <div class="row">
                <!--=================== Begin Card Content ===============-->
                <div class="card shadow no-border-radius" style="width:100% !important">
                    <div class="card-header py-3 d-flex flex-row align-items-center">
                        <h6 class="m-0 font-weight-bold card-header-text">Categories</h6>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <!-- ==================== Sub Categories form ======================-->
                            <table class="categories_table table-bordered" style="width:100%">
                                <thead>
                                    <th>S.N</th>
                                    <th>Name</th>
                                    <th>Is Parent</th>
                                    <th>Has Child</th>
                                    <th>Slug</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </thead>
                                <tbody>
                                    @foreach ($categories as $category)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{strtoUpper($category->name)}}</td>
                                            <td>{{count($category->child)>0?'Yes':'No'}}</td>
                                            <td>{{count($category->child)}}</td>
                                            <td>{{$category->slug}}</td>
                                            <td><button  type="button"  class="btn btn-primary btn-edit" id="btn-edit" data-bv-slug="{{$category->slug}}" data-bv-name="{{ ucfirst($category->name) }}" data-bv-id='{{ Hashids::connection('categories')->encode($category->id)}}' style="width: 150px; padding: 5px 0px;">
                                                    {{ __('Edit') }}
                                                </button>
                                            </td>
                                            <td><button  type="button"  class="btn btn-red btn-delete editDelete" id="editDelete" data-bv-id='{{ Hashids::connection('categories')->encode($category->id)}}' style="width: 150px; padding: 5px 0px;">
                                                    {{ __('Delete') }}
                                                </button>
                                            </td>
                                        </tr>                                        
                                    @endforeach
                                </tbody>
                            </table>
                            {{-- <ul class="drop_tree list-unstyled">
                                @foreach ($categories as $category)                                
                                    <li>
                                        @if(count($category->child))
                                            <a class="expand" href="#"><i class="fas fa-plus"></i>{{$category->name}}</a>
                                        @else
                                            <a class="" href="#">{{$category->name}}</a>
                                        @endif        
                                            <ul class="list">
                                                @foreach ($category->child as $child)
                                                    <li>
                                                        @if(count($child->child))
                                                            <a class="expand" href="#">{{$child->name}}</a>
                                                        @else
                                                            <a class="" href="#">{{$child->name}}</a>
                                                        @endif
                                                            <ul class="">
                                                                @foreach ($child->child as $child1)
                                                                <li>
                                                                    @if(count($child1->child))
                                                                        <a class="expand" href="#">{{$child1->name}}</a>
                                                                        <ul class="list-unstyled">
                                                                        @foreach ($child1->child as $child2)
                                                                            <li><a class="">{{$child2->name}}</a></li>
                                                                        @endforeach
                                                                        </ul>
                                                                    @else
                                                                        <a class="">{{$child1->name}}</a>
                                                                    @endif
                                                                </li>                                                                
                                                                @endforeach
                                                            </ul>
                                                        
                                                    </li>    
                                                @endforeach                                    
                                            </ul>                                   
                                    </li>
                                @endforeach
                            </ul> --}}
                            <!-- ==================== End Sub Categories form ======================-->
                        </div>
                    </div>
                </div>
                <!--=================== End Card Content ===============-->
            </div>
            <!--=================== End Row ===============-->
        </div>
        <!--=================== End Container Fluid ===============-->
    </div>
    <div class="modal editModal" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel">
        <div class="modal-dialog" role="document" style="margin-top: 80px;">
            <div class="modal-content no-border-radius">
                <div class="modal-header custom-header no-border-radius">
                    <h5 class="modal-title" id="editModalLabel">Edit Categories</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <form id="editForm" method="POST">
                        @csrf
                        <input type="hidden" name="_method" value="put">
                        <input type="hidden" name="id" id="category_id" value="">
                        <div class="form-group row">
                            <label class="name change_label_display" for="name">Name :<span class='text-notes'>(Notes:Please enter alphabets and ' only)</span></label>
                            <input type="text" name="name" id="name" data-bv-notempty="true" class="change_input_display form-control" style="width: 80%;">
                            <br>
                            <span class='category_name_error error_message hidden' style="padding-left: 13px;">Category Name is required.</span>
                        </div>
                        <div class="form-group row">
                            <label class="slug change_label_display" for="name">Slug :<span class='text-notes'>(Notes:Please enter alphabets and - only)</span></label>
                            <input type="text" name="slug" id="slug" data-bv-notempty="true" class="change_input_display form-control" style="width: 80%;">
                            <br>
                            <span class='category_slug_error error_message hidden' style="padding-left: 13px;">Slug is required.</span>
                        </div>
                    </form>
                </div>
                <div class="modal-footer" style="height: 50px;">
                        <input type="button" id="edit_submit_btn" class="btn btn-primary modal-submit-btn edit_submit_btn" value="Submit">
                    <button type="button" class="btn btn-primary no-border-radius modal-close-btn" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{--  error: function (request, status, error) {
        json = $.parseJSON(request.responseText);
        $.each(json.errors, function(key, value){  --}}
    @section('scripts')
    <script src="{{ asset('js/admin_js/categories.js')}}"></script>
        <script>
            $(document).ready(function()
            {
                var options={
                    "update":"{{url('admin/categories')}}",
                    "delete":"{{url('admin/categories')}}",
                    "csrf_token":'{{ csrf_token() }}'
                }
                var categoryEdit=new CategoryEdit(options);
                categoryEdit.init();
            });     
        </script>
    @endsection
@endsection
