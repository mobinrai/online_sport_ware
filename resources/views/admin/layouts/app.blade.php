<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <?php $uri="$_SERVER[REQUEST_URI]"; ?>
        <title>E-Shop Nepal Admin - {{ $uri }}</title>
        <!--=============== Custom fonts for this template=====================-->
        <link href="{{ asset('admin-libraries/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <!--=============== Custom styles for this template=====================-->
        <link href="{{ asset('admin-libraries/css/sb-admin-2.min.css') }}" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="{{ asset('css/site-style.css')}}" />
        <script src="{{ asset('admin-libraries/vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('js/common_js/ajax-helper.js')}}"></script>
         <script src="{{ asset('js/common_js/helpers.js')}}"></script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        </script>
        {{--  <script src="{{ asset('js/bootstrapvalidator/bootstrapValidatior.min.js')}}"></script>  --}}
    </head>
    <body id="page-top">
        <!--=============== Page Wrapper =====================-->
        <div id="wrapper">
            <!--=============== Sidebar =====================-->
            @include('admin.partials.menu')
            <!--=============== End of Sidebar =====================-->

            <!--=============== Content Wrapper =====================-->
            <div id="content-wrapper" class="d-flex flex-column">
                <!--================================= Topbar =======================================-->
                @include('admin.partials.top-menu')
                <!--================================= End of Topbar =================================-->
                <!--                        |
                | success and error modal   |
                |                           |-->
                @include('partials.success')
                @include('partials.error')
                <script type="text/javascript">
                    @yield('message-scripts')
                </script>
                <!--================================= Main Content ===================================-->
                @yield('content')
                <!--================================= End of Main Content =============================-->

                <!--=============== Footer =====================-->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; Your Website 2019</span>
                        </div>
                    </div>
                </footer>
                <!--=============== End of Footer =====================-->
            </div>
            <!--=============== End of Content Wrapper =====================-->
        </div>
        <!--                        |
        | ========================  |
        | End of Wrapper            |
        |========================== |
        |                           |-->
        <!--=============== Scroll to Top Button ====================-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>
        <!--=============== End Scroll to Top Button  ================-->
        <!--=============== Logout Modal =============================-->
        @include('admin.partials.logout-modal')
        <!--                        |
        | global modal              |
        |                           |-->
        @include('partials.global_modal')@include('partials.error_success_modal')
        @yield('scripts')
        <!--=============== Core plugin JavaScript=====================-->
        <script src="{{ asset('admin-libraries/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
        <!--=============== Custom scripts for all pages=====================-->
        <script src="{{ asset('admin-libraries/js/sb-admin-2.min.js')}}"></script>
        <!--=============== Page level plugins =====================-->
        <script src="{{ asset('admin-libraries/vendor/chart.js/Chart.min.js')}}"></script>
        <!--=============== Page level custom scripts =====================-->
        <script src="{{ asset('admin-libraries/js/demo/chart-area-demo.js')}}"></script>
        <script src="{{ asset('admin-libraries/js/demo/chart-pie-demo.js')}}"></script>
        <!--=============== Bootstrap core JavaScript=====================-->
        <script src="{{ asset('admin-libraries/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    </body>
</html>
