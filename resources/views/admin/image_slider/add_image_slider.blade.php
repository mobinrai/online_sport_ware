@extends('admin.layouts.app')
@section('content')
    <div id="content">
        <!--=================== Begin Page Content ===============-->
        <div class="container-fluid">
            <div class="row">
                <!--=================== Begin Card Content ===============-->
                <div class="card shadow no-border-radius col-xl-8 col-lg-7">
                    <div class="card-header py-3 d-flex flex-row align-items-center">
                        <h6 class="m-0 font-weight-bold text-primary">Add Image Slider</h6>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <form action="{{ url('/admin/image_slider/add_image_slider') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <label for="image_name" class="col-form-label text-md-right">{{ __('Image Name') }}</label>
                                        <br>
                                        <input id="image_name" type="text" class="form-control @error('image_name') is-invalid @enderror" name="image_name" value="{{ old('image_name') }}" required title="Image name should be letter and numbers" placeholder="Image name *">
                                        @if($errors->has('image_name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('image_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <label for="image" class="col-form-label text-md-right">{{ __('Image') }}</label>
                                        <br>
                                        <input id="image" type="file" class="@error('image') is-invalid @enderror" name="image" value="{{ old('image') }}" required>
                                        @if($errors->has('image'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-10">
                                        <label for="position" class="col-form-label text-md-right">{{ __('Position') }}</label>
                                        <br>
                                        <input id="position" type="text" class="form-control @error('position') is-invalid @enderror" name="position" value="{{ old('position') }}" required>
                                        @if($errors->has('position'))
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('position') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                                <div class="form-group">
                                        <div class="col-md-10">
                                            <label for="is_active" class="col-form-label">{{ __('Is Active') }}</label>
                                            <br>
                                            <input id="is_active" type="checkbox" class="@error('is_active') is-invalid @enderror" name="is_active" value="{{ old('is_active') ? old('is_active'):'1' }}" checked title="By Default image is active">
                                            @if($errors->has('is_active'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('is_active') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="col-md-2"></div>
                                    </div>
                                <div class="form-group row">
                                    <div class="col-md-10">
                                        <button  type="submit" class="btn btn-login btn-primary width-49per">
                                            {{ __('Add Image') }}
                                        </button>
                                        <button  type="button" class="btn btn-login width-49per btn-red">
                                                {{ __('Back') }}
                                            </button>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--=================== End Card Content ===============-->
            </div>
            <!--=================== End Row ===============-->
        </div>
        <!--=================== End Container Fluid ===============-->
    </div>
@endsection
