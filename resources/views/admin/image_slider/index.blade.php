@extends('admin.layouts.app')
@section('content')
    <div id="content">
        <!--=================== Begin Page Content ===============-->
        <div class="container-fluid">
            @foreach ($slider_images as $sliders)
                <div class="card bg-light shadow col-md-3">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">{{ $sliders->name }}</h6>
                    </div>
                    <div class="card-body image-slider">
                        <img src="{{ asset('public/images/slider/'.$sliders->image_path) }}" alt="">
                    </div>
                    <div class="card-footer text-muted">
                        <button type="submit" class="btn btn-red width-50per">
                            Edit
                        </button>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
