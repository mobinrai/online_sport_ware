
<!--=============== Sidebar =====================-->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <!--=============== Slidder - Toggle =====================-->
    <div class="text-center d-none d-md-inline" style="margin-top:10px">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
    <hr class="sidebar-divider my-0">
    <!--=============== Sidebar - Brand =====================-->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('/admin')}}">
        <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-laugh-wink"></i>
        </div>
        <br>
        <div class="sidebar-brand-text mx-3"></sup></div>
    </a>
    <!--=============== Divider =====================-->
    <hr class="sidebar-divider my-0">
    <!--=============== Nav Item - Dashboard =====================-->
    <li class="nav-item active">
        <a class="nav-link" href="{{ url('/admin') }}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span></span></a>
    </li>
    <!--=============== Divider =====================-->
    <hr class="sidebar-divider">
    <!--=============== Nav Item - Categories Menu =====================-->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
        <i class="fas fa-fw fa-folder"></i>
        <span>Top Categories</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Top Categories</h6>
            <a class="collapse-item" href="{{ url('/admin/top_categories') }}">List</a>
            <a class="collapse-item" href="{{ url('/admin/top_categories/add_top_categories') }}">Add</a>
        </div>
        </div>
    </li>
    <hr class="sidebar-divider">
    <!--=============== Nav Item - Categories Menu =====================-->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCategory" aria-expanded="true" aria-controls="collapseTwo">
        <i class="fas fa-fw fa-folder"></i>
        <span>Categories</span>
        </a>
        <div id="collapseCategory" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Top Categories</h6>
            <a class="collapse-item" href="{{ url('admin/categories') }}">List</a>
            <a class="collapse-item" href="{{ url('admin/categories/create') }}">Add</a>
        </div>
        </div>
    </li>
    <!--=============== Nav Item - Sub Categories Collapse Menu =====================-->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-fw fa-folder"></i>
        <span>Sub Categories</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Sub Categories :</h6>
            <a class="collapse-item" href="{{ url('admin/sub_categories/') }}">List</a>
            <a class="collapse-item" href="{{ url('admin/sub_categories/add_sub_categories') }}">Add</a>
        </div>
        </div>
    </li>
    <!--=============== Nav Item - Categories Collapse Menu =====================-->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCategories" aria-expanded="true" aria-controls="collapseCategoreis">
        <i class="fas fa-fw fa-folder"></i>
        <span>Items</span>
        </a>
        <div id="collapseCategories" class="collapse" aria-labelledby="headingCategories" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Items :</h6>
            <a class="collapse-item" href="{{url('/admin/items')}}">List</a>
            <a class="collapse-item" href="{{url('/admin/items/add_items')}}">Add</a>
        </div>
        </div>
    </li>
    <!--=============== Nav Item - Checkout List Single Menu =====================-->
    <li class="nav-item">
        <a class="nav-link" href='{{ url('/admin/users') }}'>
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Checkout List</span></a>
    </li>
    <!--=============== Nav Item - Emails Single Menu =====================-->
    <li class="nav-item">
        <a class="nav-link" href='{{ url('/admin/users') }}'>
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Emails</span></a>
    </li>
    <!--=============== Nav Item - FAQ Single Menu =====================-->
    <li class="nav-item">
        <a class="nav-link" href='{{ url('/admin/users') }}'>
        <i class="fas fa-fw fa-chart-area"></i>
        <span>FAQ</span></a>
    </li>
    <!--=============== Nav Item - Products Collapse Menu =====================-->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseProducts" aria-expanded="true" aria-controls="collapseProducts">
        <i class="fas fa-fw fa-folder"></i>
        <span>Produts</span>
        </a>
        <div id="collapseProducts" class="collapse" aria-labelledby="headingProducts" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Produts : </h6>
            <a class="collapse-item" href="buttons.html">List</a>
            <a class="collapse-item" href="buttons.html">Add</a>
            <a class="collapse-item" href="cards.html">Edit</a>
            <a class="collapse-item" href="cards.html">Delete</a>
        </div>
        </div>
    </li>
    <!--=============== Nav Item - Orders Collapse Menu =====================-->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseOrders" aria-expanded="true" aria-controls="collapseProducts">
        <i class="fas fa-fw fa-folder"></i>
        <span>Orders</span>
        </a>
        <div id="collapseOrders" class="collapse" aria-labelledby="headingOrders" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Orders : </h6>
            <a class="collapse-item" href="buttons.html">Todays Order</a>
            <a class="collapse-item" href="buttons.html">Delivered Order</a>
            <a class="collapse-item" href="cards.html">Pending Order</a>
            <a class="collapse-item" href="cards.html">Return Orders</a>
        </div>
        </div>
    </li>
    <!--=============== Nav Item - Sales Collapse Menu =====================-->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSales" aria-expanded="true" aria-controls="collapseProducts">
        <i class="fas fa-fw fa-folder"></i>
        <span>Sales</span>
        </a>
        <div id="collapseSales" class="collapse" aria-labelledby="headingSales" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Sales : </h6>
            <a class="collapse-item" href="buttons.html">List</a>
            <a class="collapse-item" href="buttons.html">Add</a>
            <a class="collapse-item" href="cards.html">Edit</a>
            <a class="collapse-item" href="cards.html">Delete</a>
        </div>
        </div>
    </li>
    <!--=============== Nav Item - Sliders Collapse Menu =====================-->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSliderImages" aria-expanded="true" aria-controls="collapseProducts">
        <i class="fas fa-fw fa-folder"></i>
        <span>Slider Images</span>
        </a>
        <div id="collapseSliderImages" class="collapse" aria-labelledby="headingSliderImages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Image Sliders: </h6>
            <a class="collapse-item" href="{{ url('/admin/image_slider/') }}">List</a>
            <a class="collapse-item" href="{{ url('/admin/image_slider/add_image_slider') }}">Add</a>
        </div>
        </div>
    </li>
    <!--=============== Nav Item - Users Single Menu =====================-->
    <li class="nav-item">
        <a class="nav-link" href='{{ url('/admin/users') }}'>
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Users</span></a>
    </li>
    <!--=============== Divider =====================-->
    <hr class="sidebar-divider">
    <!--=============== Heading =====================-->
    <div class="sidebar-heading">
        Addons
    </div>

    <!--=============== Nav Item - Pages Collapse Menu =====================-->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
        <i class="fas fa-fw fa-folder"></i>
        <span>Pages</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Login Screens:</h6>
            <a class="collapse-item" href="login.html">Login</a>
            <a class="collapse-item" href="register.html">Register</a>
            <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
            <div class="collapse-divider"></div>
            <h6 class="collapse-header">Other Pages:</h6>
            <a class="collapse-item" href="{{ url('secret/admin/error-page') }}">404 Page</a>
            <a class="collapse-item" href="blank.html">Blank Page</a>
        </div>
        </div>
    </li>

    <!--=============== Nav Item - Charts =====================-->
    <li class="nav-item">
        <a class="nav-link" href='{{ url('/admin/users-details') }}'>
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Users Details</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href='{{ url('/admin/users-details') }}'>
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Users Details</span></a>
    </li>
    <!--=============== Nav Item - Tables =====================-->
    <li class="nav-item">
        <a class="nav-link" href="tables.html">
        <i class="fas fa-fw fa-table"></i>
        <span>Tables</span></a>
    </li>

    <!--=============== Divider =====================-->
    <hr class="sidebar-divider d-none d-md-block">

    <!--=============== Sidebar Toggler (Sidebar) =====================-->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

    </ul>
    <!--=============== End of Sidebar =====================-->
