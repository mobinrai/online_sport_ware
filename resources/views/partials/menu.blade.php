<ul class="category-list">
    @foreach ($categories as $category)                                
        <li class="dropdown side-dropdown change-cursor">
            @if(count($category['child']))
                <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">{{$category->name}}<i class="fa fa-angle-right"></i></a>
                <div class="custom-menu">
                    <div class="row">
                        <div class="col-md-4" style="width: 80% !important;">
                            <ul class="">
                                @foreach ($category['child'] as $child)
                                    <li class="dropdown side-dropdown change-cursor">
                                        <a class="" href="{{ url("$category->slug/$child->slug") }}">{{strtoUpper($child->name)}}</a>                                           
                                    </li>    
                                @endforeach                                    
                            </ul>
                        </div>
                    </div>
                </div>            
            @else
                <a class="" href='{{ url("$category->slug") }}'>{{$category->name}}</a>
            @endif
        </li>
    @endforeach
</ul>