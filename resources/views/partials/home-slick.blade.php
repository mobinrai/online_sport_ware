<div id="home">
    <!--=============== container ==================================-->
    <div class="container">
        <!--=============== home wrap ==================================-->
        <div class="home-wrap">
            <!--=============== home slick ==================================-->
            <div id="home-slick">
                <!--=============== banner ==================================-->
                @foreach ($image_slider as $sliders)
                    <div class="banner banner-1">
                        <img src="{{ asset('images/slider/'.$sliders->image_path)}}" alt="">
                        <div class="banner-caption text-center">
                            <h1>Bags sale</h1>
                            <h3 class="white-color font-weak">Up to 50% Discount</h3>
                            <button class="primary-btn">Shop Now</button>
                        </div>
                    </div>
                @endforeach
            </div>
            <!--=============== /home slick ==================================-->
        </div>
        <!--=============== /home wrap ==================================-->
    </div>
    <!--=============== /container ==================================-->
</div>
