@if(isset($errors) && count($errors)>0)
    <div id="error-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Errpr Message</h3>
        </div>
        <div class="modal-body">
            <p>
                @foreach ($errors as $error)
                    {{$error}}
                @endforeach
            </p>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
    </div>
    @section('message-scripts')
        <script>
            $(document).ready(fucntion(){
                $('#error-modal').show();
            });
        </script>
    @endsection

@endif
