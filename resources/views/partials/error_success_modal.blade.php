<div class="modal" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 80px;">
        <div class="modal-content no-border-radius">
            <div class="modal-header custom-header no-border-radius">
                <h5 class="modal-title" id="myModalLabel">Error Message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer" style="height: 50px;">
                <button type="button" class="btn btn-primary no-border-radius modal-close-btn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 80px;">
        <div class="modal-content no-border-radius">
            <div class="modal-header custom-header no-border-radius">
                <h5 class="modal-title" id="myModalLabel">Success Message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer" style="height: 50px;">
                <button type="button" class="btn btn-primary no-border-radius modal-close-btn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

