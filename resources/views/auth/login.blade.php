@extends('layouts.app')

@section('content')
<div class="container login-card">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 login-inner show-shadow">
            <div class="card-deck">
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-6 login-img-container">
                            <img src="{{ asset('e-shop-libraries/img/banner11.jpg')}}" alt="" style="width:100%;height: 486px;">
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 login-form">
                            <div class=" col-md-10 card-title">
                                    <h3 class="">{{ __('Login') }}</h3>
                            </div>
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-md-10">
                                        <label for="email" class="col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                                        <br>
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-10">
                                        <label for="password" class="col-form-label text-md-right">{{ __('Password') }}</label>
                                        <br>
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                        <span><img src="" alt=""></span>

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-10">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <label class="form-check-label" for="remember">
                                                {{ __('Remember Me') }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-10">
                                        <button  type="submit" class="btn btn-login btn-primary btn-block">
                                            {{ __('Login') }}
                                        </button>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                                <div class="form-group row" style="border-top:1px solid #ccccc">
                                    <div class="col-md-10">
                                            <a href="http://"  class="btn btn-login btn-google btn-block">
                                                <i class="fa fa-google-plus" style="color:white;padding-right:2px"></i> {{ __('Login with Gmail') }}
                                            </a>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-10">
                                        <a href="http://"  class="btn btn-login btn-facebook btn-block">
                                            <i class="fa fa-facebook" style="color:white;padding-right:2px"></i> {{ __('Login with Facebook') }}
                                        </a>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                                <div class="form-group row" style="border-top:1px solid #ccccc">
                                    <div class="col-md-10">
                                        @if (Route::has('password.request'))
                                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                                {{ __('Forgot Your Password?') }}
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
