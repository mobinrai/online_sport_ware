@extends('layouts.app')
<!--=======================start of page header section=======================-->
<!--=======================end of page header section=======================-->
@section('content')
    <section class="bedroom-body">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="error-area">
                        <div class="error_inner">
                            {{-- <img src="{{ asset('e-shop-libraries/img/404-error.jpg') }}" alt=""> --}}
                            <span class="error-404">404</span>
                            <h5>Error - Not Found</h5>
                            <p>Sorrey We’re not able to find what you looking for</p>
                            <h6>back to <a href="{{ url('/') }}">Home</a></h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
