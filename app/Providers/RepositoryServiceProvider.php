<?php
//
namespace App\Http\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Repositories\BaseRepository\AdminBaseRepository;
use App\Http\Repositories\BaseRepository\Interfaces\IBaseRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind(IBaseRepositoryInterface::class,
                        AdminBaseRepository::class);           
    }
}

// $this->app->bind(
//     'App\Repositories\TopCategoryRepositoryInterface',
//     'App\Repositories\TopCategoryRepository' );
