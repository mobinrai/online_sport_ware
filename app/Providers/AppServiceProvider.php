<?php

namespace App\Providers;

use DB;
use App\Models\Category;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        //$this->app->register(BackendServiceProvider::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        DB::listen(function($query){
            logger()->info($query->sql.print_r($query->bindings,true));
        });        
        /***
         * @var topCategories is shared in all views.... 
         */        
        $categories=Category::with('child')->where(["parent_id" => 0])->get();
        view()->share(compact('categories'));
    }
}
