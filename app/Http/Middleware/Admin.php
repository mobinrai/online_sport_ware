<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Admin
{
    public function __construct()
    {
       
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$guard='admin')
    {
        if(Auth::guard($guard)->check())
        {
            $is_admin=Auth::guard($guard)->user()->is_admin;
            if(Auth::guard($guard)->check() && $is_admin=='1')
            {
                    return $next($request);
            }
        }
        else
        {
            return redirect()->route('login');
        }
    }
}
