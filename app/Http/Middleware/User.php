<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class User
{
    public function __construct()
    {
       
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$guard='user')
    {
        $userAuth=Auth::guard($guard)->check();
        if (false == $userAuth) {
            return redirect()->route('login');
        }
        return $next($request);
    }
}
