<?php 

namespace  App\Http\Repositories\BaseRepository\Interfaces;

interface IBaseRepositoryInterface
{
    /**
     * @param array $data
     * @return mix
     */
    public function create(array $attributes);
    /**
     * @param array $data
     * @return mix
     */
    public function update(array $data);
    /**
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     * @return mixed
     */
    public function all($columns = ['*'], string $orderBy = 'id', string $sortBy = 'asc');
    /**
     * @param string $id
     * @return mixed
     */
    public function find($id);
    /**
     * @param  $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findOneOrFail($id);
    /**
     * @param array $data
     * @return Collection
     */
    public function findBy(array $data);
    /**
     * @param array $data
     * @return mixed
     */
    public function findOneBy(array $data);
    /**
     * @param array $data
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findOneByOrFail(array $data);
    /**
     * @return mix
     * @throws \Exception
     */
    public function delete();
}