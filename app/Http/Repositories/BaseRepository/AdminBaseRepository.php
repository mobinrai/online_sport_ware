<?php

namespace  App\Http\Repositories\BaseRepository;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Repositories\BaseRepository\Interfaces\IBaseRepositoryInterface;

class AdminBaseRepository implements IBaseRepositoryInterface
{
    /**
    * @var Model
    */
    private $model;
    /**
    * @var result
    * @return mix
    */
    private $result;
    /**
    * @var array
    * @return mix
    */
    private $response_result;
    /**
     * BaseRepository constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
    * @param array $columns
    * @param string $orderBy
    * @param string $sortBy
    * @return mixed
    */
    public function all($columns = ['*'], string $orderBy = 'id', string $sortBy = 'asc')
    {
        try
        {
            return $this->model->orderBy($orderBy, $sortBy)->get($columns);
        }
        catch(ModelNotFoundException $ex)
        {
            $this->response_result=['success'=>false,
                            'msg'=>"Couldnot found Resource !!!"];
            return $this->response_result;
        }
        
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function find($id)
    {
        try
        {
            return $this->model->find($id);
        }
        catch(ModelNotFoundException $ex)
        {
            $this->response_result=['success'=>false,
                            'msg'=>"Couldnot found Resource !!!"];
            return $this->response_result;
        }
       
    }

    /**
     * @param  $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findOneOrFail($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param array $data
     * @return Collection
     */
    public function findBy(array $data)
    {
        return $this->model->where($data)->get();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function findOneBy(array $data)
    {
        return $this->model->where($data)->first();
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function findOneByOrFail(array $data)
    {
        return $this->model->where($data)->firstOrFail();
    }

    public function updateWithWhereIn($ids,array $data){
        try
        {
            $this->result=$this->model->whereIn('id',$ids)->update($data);
            if($this->result>0){
                $this->response_result=['success'=>true,
                'msg'=>"Successfully Updated!!!"];
                return $this->response_result;
            }
            else
            {
                $this->response_result=['success'=>false,
                            'msg'=>"Couldnot Update Row!!!"];
                return $this->response_result;
            }
        }
        catch(ModelNotFoundException $ex)
        {
            $this->response_result=['success'=>false,
                            'msg'=>"Couldnot found Resource !!!"];
            return $this->response_result;
        }
    }
    /**
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes)
    {
        try
        {
            $this->result=$this->model->create($attributes);

            if($this->result->id)
            {
                $this->response_result=['success'=>true,
                            'msg'=>"Successfully Added New Data !!!"];
                return $this->response_result;
            }
            else
            {
                $this->response_result=['success'=>false,
                            'msg'=>"Couldnot Added New Data !!!"];
                return $this->response_result;
            }
        }
        catch(ModelNotFoundException $ex)
        {
            $this->response_result=['success'=>false,
                            'msg'=>"Couldnot found Resource !!!"];
            return $this->response_result;
        }
    }
    /**
     * @param array $data
     * @return bool
     */
    public function update(array $data) 
    {
        try
        {
            $this->result   = $this->model->update($data);
            if($this->result)
            {
                $this->response_result=['success'=>true,
                            'msg'=>"Successfully Updated!!!"];
                return $this->response_result;
            }
            else
            {
                $this->response_result=['success'=>false,
                            'msg'=>"Couldnot Update Row!!!"];
                return $this->response_result;
            }
        }
        catch(ModelNotFoundException $ex)
        {
            $this->response_result=['success'=>false,
                            'msg'=>"Couldnot found Resource !!!"];
                return $this->response_result;
        }
        
    }
    /**
     * @return bool
     * @throws \Exception
     */
    public function delete() 
    {
        try
        {
            $this->result   = $this->model->delete();
            if($this->result)
            {
                $this->response_result=['success'=>true,
                            'msg'=>"Successfully Deleted !!!"];
           }
            else
            {
                $this->response_result=['success'=>false,
                            'msg'=>"Couldnot Delete Row!!!"];
            }
        }
        catch(ModelNotFoundException $ex)
        {
            $this->response_result=['success'=>false,
                            'msg'=>"Couldnot found Resource !!!"];
                
        }
        return $this->response_result;
    }
    public function insertQueryTransaction(){
        DB::beginTransaction();
        try
        {
            DB::insert();
            DB::commit();
        }catch(\Exception $ex){
            DB::rollback();
            $this->response_result=['success'=>false,
                            'msg'=>"Couldnot found Resource !!!"];
        }
    }
}