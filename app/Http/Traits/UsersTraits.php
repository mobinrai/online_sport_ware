<?php
    namespace App\Http\Traits;
    use Auth;
    trait UsersTraits
    {
        public function checkUserLogin()
        {
            $admincheck= Auth::guard('admin')->check();
            $usercheck = Auth::guard('user')->check();
            if(!$admincheck && !$usercheck)
            {
                return false;
            }
            if($admincheck)
            {
                return $name='admin/';
            }
            if($usercheck)
            {
                return $name='users/';
            }
        }       
        
    }