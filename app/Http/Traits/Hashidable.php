<?php
namespace App\Http\Traits;

trait Hashidable
{
    public function decodeId($class_name,$key)
    {
        $id     = \Hashids::connection($class_name)->decode($key)[0] ?? null;
        return $id;
    }
    public function getById($class_name,$id)
    {
        $array = $class_name::findOrfail($id);
        return $array;
    }
}
?>
