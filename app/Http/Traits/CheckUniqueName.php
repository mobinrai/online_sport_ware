<?php
    namespace App\Http\Traits;

    trait CheckUniqueName
    {
       
        public function checkUniqueName($className,array $list)
        {
            $result = $className::where($list)->get();

            if ($result->count()) 
            {
                return true;
            }
            return false;
        }
    }