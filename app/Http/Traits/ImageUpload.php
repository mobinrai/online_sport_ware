<?php
    namespace App\Http\Traits;
    use Illuminate\Http\UploadedFile;
    use Illuminate\Support\Facades\Storage;

    trait ImageUpload
    {
        public function imageUpload($image,$folder='img')
        {
            $image_name     = str_random(20);
            $ext            = strtolower($image->getClientOriginalExtension());
            $image_full_name= $image_name.'.'.$ext;

            $destination_path= public_path("/images/{$folder}");
            $success         = $image->move($destination_path,$image_full_name);
            return $image_full_name;
        }
    }
?>
