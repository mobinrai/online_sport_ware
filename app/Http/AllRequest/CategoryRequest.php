<?php

namespace App\Http\AllRequest;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'required|regex:/^[a-zA-Z \']+$/|unique:categories,name',
            'slug'         => 'required|regex:/^[a-zA-Z -]+$/|unique:categories,slug',            
        ];
    }
    public function message(){
        return [
            'name.required' => 'Item name is required.',
            'name.regex'    => 'Invalid Item Name.',
            'name.unique'   => 'Name has already taken',
            'slug.required'=>  'Slug is required',

        ];
    }
}
