<?php

namespace App\Http\AllRequest;

use Illuminate\Foundation\Http\FormRequest;

class ItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item_name'         => 'required|regex:/^[a-zA-Z \']+$/|unique:items,name',
            'top_category_id'   => 'required',
            'price'             => 'requred|regex:/^[0-9.]+$/',
            'quantity'          => 'required|regex:/^[0-9]+$/',
            'image'             => 'required|image|mimes:jpeg,jpg|max:2048'
            //'slug'=>'required|regex:/^[a-zA-Z \']+$/'
        ];
    }
    public function message(){
        return [
            'item_name.required'        => 'Item name is required.',
            'item.regex'                => 'Invalid Item Name.',
            'top_category_id.required'  => 'Top Category is required.',
            'image.mins'                => 'Image format can only be jpeg and jpg.',
            'image.max'                 => 'Image size cannot be greater than 2048 kb.',
            'price.required'            => 'Item Price is required.',
            'price.regex'               => 'Invalid Item Price.',
            'quantity.regex'            => 'Invalid Item Quantity.',

        ];
    }
}
