<?php

namespace App\Http\AllRequest;

use Illuminate\Foundation\Http\FormRequest;

class SubCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array(
                'name' => ['required', 'string', 'regex:/^[a-z A-Z- \']+$/', 'max:25'],
                'top_categories_list'=>['required']);


    }
    public function message()
    {
        return [
            'name.required' => 'Name is required',
            'name.regex'    => 'Name is not valid',
            'top_categories_list.required'=>'Select Top Category'
        ];
    }
}
