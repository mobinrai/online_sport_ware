<?php

namespace App\Http\AllRequest;

use Illuminate\Foundation\Http\FormRequest;

class SliderImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image_name' => 'required|regex:/^[a-zA-Z 0-9-_]+$/',
            'image'      => 'required|image|mimes:jpeg,jpg|max:2048',
            'position'   => 'required|numeric',
        ];
    }
    public function message()
    {
        return [
            'image_name.required'   => 'Image name is required',
            'image.required'        => 'Image cannot be empty',
            'image.mins'            => 'Image format can only be jpeg and jpg.',
            'image.max'             => 'Image size cannot be greater than 2048 kb.',
            'position.required'     => 'Image Position is required',

        ];
    }
}
