<?php

namespace App\Http\Controllers\Admin;

use App\Models\Items;
use App\Models\SubCategory;
use App\Models\TopCategory;
use App\Http\Traits\Hashidable;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\AllRequest\ItemRequest;
use App\Http\Repositories\BaseRepository\AdminBaseRepository;
class ItemsController extends Controller
{
    //
    use Hashidable;
    /**
     *@var 
     */
    private $baseRepo;
    
    public function __construct()
    {
        $items =new Items();
        $this->baseRepo=new AdminBaseRepository($items);
    }
    public function index()
    {
        return view('admin.items.index');
    }
    public function add()
    {
        $top_categories=TopCategory::all();
        $sub_categories=SubCategory::all();
        return view('admin.items.add_items',array('top_categories'=>$top_categories,'sub_categories'=>$sub_categories));
    }
    public function createItem(ItemRequest $request)
    {
        
    }
}
