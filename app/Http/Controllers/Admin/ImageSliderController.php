<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ImageSliders;
use App\Http\Traits\ImageUpload;
use App\Http\AllRequest\SliderImageRequest;
use Auth;
use DB;

class ImageSliderController extends Controller
{
    use ImageUpload;

    private $user_id;

    public function __construct()
    {
        //$this->user_id    = Auth::id();
        $this->user_id    = 1;
    }
    public function addImages()
    {
        return view('admin.image_slider.add_image_slider');
    }
    public function listimages()
    {
        // $imageSlider= ImageSliders::where('user_id','=',$this->user_id)
        //                             ->orderBy('position','asc');
        $imageSlider = DB::select('SELECT * FROM e_shop_nepal.image_sliders where user_id=?',[$this->user_id]);
        return view('admin.image_slider.index',['slider_images' => $imageSlider]);
    }
    public function store(SliderImageRequest $request)
    {
        $request->validated();
        try
        {
            if($request->hasFile('image'))
            {
                if($request->file('image')->isValid())
                {
                    $file_name  = $this->imageUpload($request->file('image'),'slider');
                    $succes     = ImageSliders::create([
                        'name'      => $request->get('image_name'),
                        'image_path'=> $file_name,
                        'user_id'   => $this->user_id,
                        'position'  => $request->get('position'),
                        'is_active' => $request->get('is_active')
                    ]);
                    if($succes)
                    {
                        return view('admin.image_slider.add_image_slider')->with('success','Uploaded Successsfully..');
                    }
                }
            }
        }catch(Exception $ex)
        {
            return Redirect::back()->withErrors($ex->getMessage())->withInput();
        }
    }
    public function edit()
    {

    }
}
