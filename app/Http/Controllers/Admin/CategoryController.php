<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Traits\Hashidable;
use App\Http\Controllers\Controller;
use App\Http\Repositories\BaseRepository\AdminBaseRepository;
use Carbon\Carbon;
use App\Http\AllRequest\CategoryRequest;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    use Hashidable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $categories;

    private $categoryWithChild;
    /**
     * @return null
     */
    private $list;

    private $adminBaseRepo;

    public function __construct()
    {
        $category=new Category();
        $this->adminBaseRepo = new AdminBaseRepository($category);
        $this->categories=$this->adminBaseRepo->findBy(['is_deleted'=>'0']);
        // $this->categoryWithChild=Category::with('child')->get();
    }
    public function index()
    {       
        //$categories=Category::with('child')->where(["parent_id" => 0])->get();        
        return view('admin.categories.index',['categories'=>$this->categories]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.add_categories_form',['parentCategories'=>$this->categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $request->validated();
        $parent_id='0';
        if($request->post('category_type')=='child')
        {
            $parent_id = $this->decodeId('categories',$request->post('category_parent_name'));
            if($parent_id==null)
            {
                return response()->json(['success'=>false,'msg'=>'Could not find Row !!!']);
            }            
        }
        $this->list=['name'=>$request->post('name'),
                    'parent_id'=>$parent_id,
                    'slug'=>Str::slug($request->post('slug')),
                    'is_deleted'=>'0',
                    'created_at'=>Carbon::now()
                ];

        $result =  $this->adminBaseRepo->create($this->list);
        
        return response()->json($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryRequest $request)
    {
        // $category=Category::where('id',$request->id);
        // return view('admin.categories.show',['category'=>$this->categoryWithParent]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryRequest $request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request,$id)
    {
        //
        //$request->validated();
        $cat_id = $this->decodeId('categories',$request->post('id'));
        if($id==null)
        {
            return response()->json(['success'=>false,'msg'=>'Could not find Row !!!']);
        }
        $this->list = [
                'id'=>$cat_id,
                'name'=>$request->post('name'),
                'slug'=>$request->post('slug'),
            ];    
        
        $findOneByOrFail= $this->adminBaseRepo->findOneOrFail($cat_id);
        $updateRepo     = new AdminBaseRepository($findOneByOrFail);         
        $updateResult   = $updateRepo->update($this->list);        
        return response()->json($updateResult);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $catId             = $this->decodeId('categories',$id);
        if($catId==null)
        {
            return response()->json(['success'=>false,'msg'=>'Could not find Row !!!']);
        }            
        $categoriesList=$this->getChildren($catId);
        $result= $this->adminBaseRepo->updateWithWhereIn($categoriesList,['is_deleted'=>'1','deleted_at'=>Carbon::now()]);
        
        return response()->json($result);
    }
   
    public static function getChildren($category_id,$categoryList=[])
    {
        $categories=Category::with('child')->where(["parent_id" => $category_id,'is_deleted'=>'0'])->get();

        foreach($categories as $category)
        {      
            if(count($category->child)>0)
            {
                $categoryList=self::getChildren($category->id,$categoryList);
            }
            else{
                array_push($categoryList,$category->id);
            }            
        }
        array_push($categoryList,$category_id);
        return $categoryList;
    }
    private  function checkCategoryName($post_data) : bool
    {
        return $this->checkUniqueName(Category::class,['name'=>trim($post_data['name'])]);
    }
}
