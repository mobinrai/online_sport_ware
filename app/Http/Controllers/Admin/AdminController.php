<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Session;

class AdminController extends Controller
{
    //
    public function __construct()
    {
      
    }
    public function index()
    {
        //dd(Auth::user());
        //$role=Auth::guard('admin')->user();
        return view('admin.pages.index');
    }
    public function showErrorPAge()
    {
        return view('admin.pages.404');
    }
    public function showUsersDetails()
    {
        return view('admin.pages.user-details');
    }
}
