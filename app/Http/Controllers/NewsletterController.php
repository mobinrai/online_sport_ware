<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Newsletter;
class NewsletterController extends Controller
{
    //
    public function __construct(){

    }
    public function create(){

    }

    public function store(Request $request)
    {
        if ( ! Newsletter::isSubscribed($request->email) ) 
        {
            Newsletter::subscribePending($request->email);
            return response()->json(['success'=>true,'msg'=>'Thank you for subscribed !!! ']);
        } 
        return response()->json(['success'=>false,'msg'=>'Sorry !! You have already subscribed !!! ']);   
    }
}

