<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Controllers\Controller;
use DB;

class FrontController extends Controller
{
    public function __construct()
    {
       
    }
    //
    public function index()
    {
        $imageSlider    = DB::select('SELECT * FROM online_shop_ware.image_sliders where is_active=1 order by position asc');
       
        return view('index',array('image_slider'=>$imageSlider));
    }
    public function products($slug1='',$slug2='',$slug3='')
    {
        $list=Category::where('slug',$slug1)->get();
        return view('front.products',array('menuList'=>$list,'slug1'=>$slug1,'slug2'=>$slug2));
    }
}
