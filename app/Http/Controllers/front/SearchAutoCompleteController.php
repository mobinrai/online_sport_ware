<?php

namespace App\Http\Controllers\front;

use DB;
use App\Models\TopCategory;
use App\Models\Items;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class SearchAutoCompleteController extends Controller
{
    public function index()
    {

    }
    public function autocomplete(Request $request)
    {
        $data = DB::table("items")
                ->when(Input::has('cat_id'), function ($query) {
                    return $query->where('cat_id', Input::get('cat-id'));
                })
                ->where("name","LIKE","%{$request->input('query')}%")
                ->get();
   
        return response()->json($data);
    }
}