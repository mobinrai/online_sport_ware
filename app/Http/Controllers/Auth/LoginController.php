<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Traits\UsersTraits;
use Illuminate\Support\MessageBag;

class LoginController extends Controller
{
    use UsersTraits;
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $redirectTo = '/users';

    private $is_admin='1';

    private $is_active='1';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {}
    
    /**
     * overwriting AuthenticatesUsers traits login function
     */
    public function login(Request $request)
    {
        // Validate the form data
        
        $validator = $this->validate($request, [
         'email'   => 'required|email',
         'password' => 'required|string'
         ]);
        // Attempt to log the customer in
         if (Auth::guard('admin')->attempt(['admin_email' => $request->email, 'password' => $request->password,'is_active'=>$this->is_active], $request->remember)) {
         // if successful, then redirect to their intended location
//             $is_admin=Auth::guard('admin')->user();
             $user=Auth::guard('admin')->user();
            if($user->is_admin==$this->is_admin)
            {
               return redirect('admin');
            }
            else
            {
                return redirect()->back()->withErrors('not Authorized to view as admin')->withInput($request->only('email', 'password'));
            }
        } 
        //attempt to log the seller in
         else if (Auth::guard('user')->attempt(['email' => $request->email, 'password' => $request->password,'is_active'=>$this->is_active], $request->remember)) {
        // if successful, then redirect to their intended location
             return redirect()->intended(route('/users'));
         }
        

         // if Auth::attempt fails (wrong credentials) create a new message bag instance.
         $errors = new MessageBag(['password' => ['Adresse email et/ou mot de passe incorrect.']]);
          // redirect back to the login page, using ->withErrors($errors) you send the error created above
         return redirect()->back()->withErrors($errors)->withInput($request->only('email', 'password'));
    }
    /**
     * overwriting AuthenticatesUsers traits login function
     */
    public function showLoginForm()
    {
        $checkUserLogin= $this->checkUserLogin('auth.login');
         if(!$checkUserLogin)
        {
            return view('auth.login');
        }
        return redirect($checkUserLogin);
    }
}
