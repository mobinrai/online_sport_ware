<?php 

namespace App\Http\Controllers\ApiAuth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use JWTFactory;
use JWTAuth;
use App\User;
use Illuminate\Support\Facades\Auth;
class ApiLoginController extends Controller
{

    public function login(Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails())
        {
            return response()->json($validator->errors());
        }
        try
        {
            if(!$token = JWTAuth::attempt(['email' => $request->email, 'password' => $request->password,'is_active'=>1]))
            {
                return response()->json(['msg'=>'invalid email/password'],401);
            }
        }
        catch(\JWTException $e)
        {
            return response()->json(['msg'=>'could not create token'],500);
        }
        return response()->json(compact('token'));
    }
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email'     => ['required', 'string', 'email', 'max:255'],
            'password'  => ['required', 'string', 'min:6'],
        ]);
    }
    public function register(Request $request)
    {
        $validator = $this->validator1($request->all());

        if($validator->fails())
        {
            return response()->json($validator->errors());
        }

        $user = $this->create($request->all());
        if(!$user)
        {
            return response()->json(['success'=>false,'msg'=>'couldnot register user.']);
        }
        if (Auth::guard('api')->attempt(['email' => $request->email, 'password' => $request->password,'is_active'=>1])) {
            
            $user = User::first();
            $token = JWTAuth::fromUser($user);
            
            return Response::json(compact('token'));
        }
    }
    protected function validator1(array $data)
    {
        return Validator::make($data, [
            'name'      => ['required', 'string', 'max:255'],
            'email'     => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password'  => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        
        return User::create([
            'user_name' => $data['name'],
            'email'     => $data['email'],
            'password'  => Hash::make($data['password']),
            'role_id'   =>'1',
            'is_active' =>'1'
        ]);
    }
    // public function getJWTIdentifier()
    // {
    //     return $this->getKey();
    // }

    // public function getJWTCustomClaims()
    // {
    //     return [];
    // }
    // public function setPasswordAttribute($password)
    // {
    //     if ( !empty($password) ) {
    //         $this->attributes['password'] = bcrypt($password);
    //     }
    // }  
    
}