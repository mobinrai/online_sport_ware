<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    //use Illuminate\Database\Eloquent\HasRecursiveRelationships;
    //
    //use SoftDeletes;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    protected $fillable=['name','parent_id','is_deleted','slug']; 

    public function parent()
    {
        return $this->hasMany('App\Models\Category', 'parent_id')->where('parent_id',0)->with('parent');
    }
    public function child()
    {
        return $this->hasMany('App\Models\Category', 'parent_id')->with('child');
    }
    // public static function boot()
    // {
    //     parent::boot();

    //     static::deleting(function($category) { 
    //         $category->child()->delete();

    //     });
    // }
    public static function tree($categories)
    {
        foreach($categories as $category){
            if(!$category->child->isEmpty()){
                $category->child=selft::tree($category->child);
            }else{
                $categories=$category;
            }
        }
        return $categories;
    }
   
    
}
