<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    //
    protected $fillable=['name','top_category_id','user_id'];

    public function topCategories()
    {
        return $this->belongsTo(Topcategory::class,'top_category_id');
    }
    public function Categories()
    {
        return $this->hasMany(Topcategory::class);
    }
    public function products()
    {
        return $this->hasMany(Products::class);
    }
    public function getTopCategoryNameAttribute()
     {
           return $this->topCategories()->name;
     }
}
