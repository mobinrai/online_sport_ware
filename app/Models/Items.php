<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    //
    protected $fillable=[ 'item_name','price','quantity','top_category_id','sub_category_id'];

    public function topCategory()
    {
        return $this->belongsTo(\App\TopCategory::class,'top_category_id');
    }
    public function subCategory()
    {
        return $this->belongsTo(\App\SubCategory::class,'sub_category_id');
    }
}
