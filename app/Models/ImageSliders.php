<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImageSliders extends Model
{
    //
    protected $fillable=['name','image_path','user_id','position','is_active'];
}
