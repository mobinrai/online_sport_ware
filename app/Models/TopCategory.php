<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TopCategory extends Model
{
    //
    protected $fillable=['name','user_id'];

    public function subCategories()
    {
        return $this->hasMany(Subcategory::class);
    }
    public function items()
    {
        return $this->hasMany(Items::class);
    }

}
