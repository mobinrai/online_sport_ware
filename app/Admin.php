<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guard='admin';
    
    protected $fillable = [
        'name', 'admin_email', 'password','is_active','is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

//    public function roles()
//    {
//        return $this->belongsTo('App\Models\Role','role_id','role_id');
//    }
//    public function hasRole($role)
//    {
//      if ($this->roles()->where('role_name', $role)->first()) {
//        return true;
//      }
//      return false;
//    }
//    public function scopeActive($query)
//    {
//        return $query->where('active', '=', 1);
//    }

}
